import * as React from 'react';
import { StyleSheet, ImageBackground } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Main from '../components/Main';
import { View } from '../components/Themed';
const bgButton = require('../assets/images/forecast-Bg-buttom.png');
const bgUp = require('../assets/images/Up-Bg-mob.png');

export default function MainScreen() {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={bgButton}
        resizeMode='cover'
        style={styles.bgButton}>
        <ImageBackground source={bgUp} resizeMode='cover' style={styles.bgUp}>
          <ScrollView>
            <Header />
            <Main />
            <Footer />
          </ScrollView>
        </ImageBackground>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#373af5',
    width: '100%',
  },
  bgUp: {
    top: 0,
    right: 0,
    flex: 1,
  },

  bgButton: {
    flex: 1,
  },
});
