import React from 'react';
import { View } from './Themed';
import { StyleSheet } from 'react-native';
import ForecastWeek from './ForecastWeek';
import ForecastDay from './ForecastDay';

const Main: React.FC = () => {
  return (
    <View style={styles.main}>
      <ForecastWeek />
      <ForecastDay />
    </View>
  );
};

export default Main;

const styles = StyleSheet.create({
  main: {
    backgroundColor: 'transparent',
  },
});
