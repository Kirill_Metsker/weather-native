import React from 'react';
import { Text, View } from './Themed';
import { StyleSheet } from 'react-native';

const Footer: React.FC = () => {
  return (
    <View style={styles.footer}>
      <Text style={styles.footer__text}>C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT</Text>
    </View>
  );
};

export default Footer;

const styles = StyleSheet.create({
  footer: {
    justifyContent: 'center',
    paddingBottom: 16,
    backgroundColor: 'transparent',
  },
  footer__text: {
    color: '#FFF',
    opacity: 0.6,
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    margin: 0,
  },
});
