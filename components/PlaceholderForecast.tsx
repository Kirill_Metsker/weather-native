import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import { Image, StyleSheet } from 'react-native';
import { Text, View } from './Themed';
const imgPlaceholder = require('../assets/images/placeholder-img.png');

const PlaceholderForecast: React.FC = () => {
  return (
    <View style={styles.placeholder}>
      <Image style={styles.placeholder__img} source={imgPlaceholder} />
      <Text style={styles.placeholder__text}>
        Fill in all the fields and the weather will be displayed
      </Text>
    </View>
  );
};

export default PlaceholderForecast;

const styles = StyleSheet.create({
  placeholder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  placeholder__text: {
    color: '#8083A4',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  placeholder__img: {
    height: 160,
    width: 160,
    marginBottom: 24,
  },
});
