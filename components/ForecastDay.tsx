import React, { useCallback, useEffect, useState } from 'react';
import { formatDateString, formatFullDate } from '../utils/formatDate';
import {
  WeatherHistoricalApiType,
  WeatherHourlyApiType,
} from '../utils/WeatherApiType';
import { View, Text } from './Themed';
import { Button, StyleSheet, TouchableOpacity, Image } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Picker } from '@react-native-picker/picker';
import { HOUR_HISTORICAL_WEATHER, IMG_WEATHER, list_cities } from '../config';
import { formatDegress } from '../utils/formatDegress';
import api from '../utils/WeatherApi';
import PlaceholderForecast from './PlaceholderForecast';

const ForecastDay: React.FC = () => {
  const [indexCity, setIndexCity] = useState<number>(0);
  const [forecastDay, setForecastDay] = useState<WeatherHourlyApiType | null>(
    null,
  );
  const [show, setShow] = useState(false);
  const [date, setDate] = useState(new Date(Date.now()));
  const [textDate, setTextDate] = useState<string>(
    formatDateString(date.getTime()),
  );

  const onChange = (event: any, selectedDate: any) => {
    const currentDate = selectedDate || date;
    setDate(currentDate);
    setTextDate(formatDateString(currentDate));
    setShow(false);
  };

  const showDatepicker = () => {
    setShow(true);
  };
  /**
   * Загружаем данны с сервера
   */
  useEffect(() => {
    if (indexCity >= 0 && date) {
      const currentDate = new Date(date).getTime();
      const nowDate = new Date();
      const maxPermissibleDate = new Date().setDate(nowDate.getDate() - 1);
      const minPermissibleDate = new Date(
        new Date().setHours(0, 0, 0, 0),
      ).setDate(nowDate.getDate() - 5);

      if (
        minPermissibleDate <= currentDate &&
        currentDate < maxPermissibleDate
      ) {
        // setIsLoading(true); // Отображение загрузки данных
        const dateUnix = Math.floor(currentDate / 1000);
        api
          .getForecastDay(list_cities[indexCity], dateUnix)
          .then((res: WeatherHistoricalApiType) => {
            setForecastDay(res.hourly[HOUR_HISTORICAL_WEATHER]);
          })
          .catch((err) => {
            console.log(err);
          });
      } else setForecastDay(null);
      // .finally(() => {
      //   setIsLoading(false); // Отображение загрузки данных
      // });
    } else setForecastDay(null);
  }, [date, indexCity]);

  return (
    <View style={styles.forecastDay}>
      <Text style={styles.forecastDay__title}>
        Forecast for a Date in the Past
      </Text>
      <View style={styles.forecastDay__menu}>
        <View style={styles.forecastDay__wrapper}>
          <Picker
            style={styles.forecastDay__select}
            selectedValue={list_cities[indexCity].name}
            onValueChange={(itemValue, itemIndex) => {
              setIndexCity(itemIndex);
            }}>
            <Picker.Item
              label={list_cities[0].name}
              value={list_cities[0].name}
            />
            <Picker.Item
              label={list_cities[1].name}
              value={list_cities[1].name}
            />
            <Picker.Item
              label={list_cities[2].name}
              value={list_cities[2].name}
            />
            <Picker.Item
              label={list_cities[3].name}
              value={list_cities[3].name}
            />
            <Picker.Item
              label={list_cities[4].name}
              value={list_cities[4].name}
            />
          </Picker>
        </View>
        <View>
          <Text style={styles.forecastDay__datePcker} onPress={showDatepicker}>
            {textDate}
          </Text>
        </View>
        {show && (
          <DateTimePicker
            testID='dateTimePicker'
            value={date}
            mode={'date'}
            is24Hour={true}
            display='default'
            onChange={onChange}
          />
        )}
      </View>
      {forecastDay ? (
        <View style={styles.forecastDay__card}>
          <Text style={styles.forecastDay__date}>
            {formatFullDate(forecastDay.dt)}
          </Text>
          <Image
            style={styles.forecastDay__img}
            source={IMG_WEATHER[forecastDay.weather[0].icon]}
          />
          <Text style={styles.forecastDay__degree}>
            {formatDegress(forecastDay.temp)}°
          </Text>
        </View>
      ) : (
        <PlaceholderForecast />
      )}
    </View>
  );
};

export default ForecastDay;

const styles = StyleSheet.create({
  forecastDay: {
    flex: 1,
    width: 300,
    height: 576,
    paddingTop: 32,
    paddingHorizontal: 24,
    paddingBottom: 60,
    marginBottom: 78,
    backgroundColor: '#fff',
    borderRadius: 8,
  },
  forecastDay__title: {
    marginBottom: 32,
    fontSize: 32,
    fontWeight: 'bold',
  },
  forecastDay__wrapper: {
    borderWidth: 2,
    backgroundColor: 'rgba(128, 131, 164, 0.06)',
    borderColor: 'rgba(128, 131, 164, 0.2)',
    marginBottom: 20,
    borderRadius: 8,
  },
  forecastDay__menu: {
    marginBottom: 20,
  },
  forecastDay__select: {
    width: 252,
    height: 48,
    fontWeight: '400',
  },
  forecastDay__card: {
    position: 'relative',
    height: 238,
    padding: 20,
    backgroundColor: '#373AF5',
    borderRadius: 8,
  },
  forecastDay__date: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
  },
  forecastDay__degree: {
    fontSize: 32,
    color: '#fff',
    alignSelf: 'flex-end',
  },
  forecastDay__img: {
    height: 150,
    width: 150,
    alignSelf: 'center',
  },
  forecastDay__datePcker: {
    marginBottom: 20,
    borderWidth: 2,
    backgroundColor: 'rgba(128, 131, 164, 0.06)',
    borderColor: 'rgba(128, 131, 164, 0.2)',
    borderRadius: 8,
    fontSize: 16,
    lineHeight: 24,
    fontWeight: 'normal',
    paddingHorizontal: 12,
    paddingVertical: 12,
    height: 48,
    width: 252,
  },
});
