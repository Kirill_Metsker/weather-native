import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import { StyleSheet } from 'react-native';
import { Text, View } from './Themed';

const Header: React.FC = () => {
  return (
    <View style={styles.header}>
      <View style={styles.headerTitle}>
        <Text style={styles.headerTitle_leftUp}>Weather</Text>
        <Text style={styles.headerTitle_rightDown}>forecast</Text>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 30,
    marginBottom: 21,
  },
  headerTitle: {
    position: 'relative',
    height: 68,
    backgroundColor: 'transparent',
  },
  title: {
    backgroundColor: 'transparent',
  },
  headerTitle_leftUp: {
    fontSize: 32,
    fontWeight: 'bold',
    color: '#fff',
    position: 'absolute',
    right: 0,
    top: 0,
  },
  headerTitle_rightDown: {
    fontSize: 32,
    fontWeight: 'bold',
    color: '#fff',
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
});
