import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import Colors from '../constants/Colors';
import { WeatherDayApiType } from '../utils/WeatherApiType';
import CardSlider from './CardSlider';
import { MonoText } from './StyledText';
import { Text, View } from './Themed';

type SliderType = {
  forecastWeek: WeatherDayApiType[];
};

const Slider: React.FC<SliderType> = ({ forecastWeek }) => {
  return (
    <View style={styles.slider}>
      <View style={styles.slider__wrapper}>
        <ScrollView horizontal={true}>
          {forecastWeek.map((day) => (
            <CardSlider day={day} key={day.dt} />
          ))}
        </ScrollView>
      </View>
    </View>
  );
};

export default Slider;

const styles = StyleSheet.create({
  slider: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 30,
    marginBottom: 21,
    height: 238,
  },
  slider__wrapper: {
    flexDirection: 'row',
    height: 238,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  link: {
    marginTop: 15,
    paddingVertical: 15,
  },
  linkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
