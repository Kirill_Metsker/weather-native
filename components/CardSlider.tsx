import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import { Image, StyleSheet } from 'react-native';
import { IMG_WEATHER } from '../config';
import { formatFullDate } from '../utils/formatDate';
import { formatDegress } from '../utils/formatDegress';
import { WeatherDayApiType } from '../utils/WeatherApiType';
import { Text, View } from './Themed';

type CardSliderType = {
  day: WeatherDayApiType;
};

const CardSlider: React.FC<CardSliderType> = ({ day }) => {
  return (
    <View key={day.dt} style={styles.cardSlider}>
      <Text style={styles.cardSlider__date}>{formatFullDate(day.dt)}</Text>
      <Image
        style={styles.cardSlider__img}
        source={IMG_WEATHER[day.weather[0].icon]}
      />
      <Text style={styles.cardSlider__degree}>
        {formatDegress(day.temp.day)}°
      </Text>
    </View>
  );
};

export default CardSlider;

const styles = StyleSheet.create({
  cardSlider: {
    flex: 1,
    backgroundColor: '#373af5',
    height: 238,
    width: 174,
    marginRight: 16,
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderRadius: 8,
  },
  cardSlider__date: {
    fontSize: 16,
    color: '#fff',
  },
  cardSlider__degree: {
    color: '#fff',
    fontSize: 32,
    alignSelf: 'flex-end',
  },
  cardSlider__img: {
    height: 120,
    width: 120,
    alignSelf: 'center',
  },
});
