import React, { useEffect, useState } from 'react';
import { list_cities } from '../config';
import api from '../utils/WeatherApi';
import { Text, View } from './Themed';
import { StyleSheet } from 'react-native';
import { WeatherDayApiType, WeatherWeakApiType } from '../utils/WeatherApiType';
import { Picker } from '@react-native-picker/picker';
import Slider from './Slider';
import PlaceholderForecast from './PlaceholderForecast';

const ForecastWeek: React.FC = () => {
  const [indexCity, setIndexCity] = useState<number>(0);
  const [forecastWeek, setForecastWeek] = useState<WeatherDayApiType[] | null>(
    null,
  );
  /**
   * Загружаем данны с сервера
   */
  useEffect(() => {
    if (indexCity >= 0) {
      api
        .getForecastWeek(list_cities[indexCity])
        .then((res: WeatherWeakApiType) => {
          setForecastWeek(res.daily);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [indexCity]);

  return (
    <View style={styles.forecastWeek}>
      <Text style={styles.forecastWeek__title}>7 Days Forecast</Text>
      <View style={styles.forecastWeek__menu}>
        <View style={styles.forecastWeek__wrapper}>
          <Picker
            style={styles.select}
            selectedValue={list_cities[indexCity].name}
            onValueChange={(itemValue, itemIndex) => {
              setIndexCity(itemIndex);
            }}>
            <Picker.Item
              label={list_cities[0].name}
              value={list_cities[0].name}
            />
            <Picker.Item
              label={list_cities[1].name}
              value={list_cities[1].name}
            />
            <Picker.Item
              label={list_cities[2].name}
              value={list_cities[2].name}
            />
            <Picker.Item
              label={list_cities[3].name}
              value={list_cities[3].name}
            />
            <Picker.Item
              label={list_cities[4].name}
              value={list_cities[4].name}
            />
          </Picker>
        </View>
      </View>

      {forecastWeek ? (
        <Slider forecastWeek={forecastWeek} />
      ) : (
        <PlaceholderForecast />
      )}
    </View>
  );
};

export default ForecastWeek;

const styles = StyleSheet.create({
  forecastWeek: {
    flex: 1,
    width: 300,
    height: 464,
    paddingTop: 32,
    paddingHorizontal: 24,
    paddingBottom: 60,
    marginBottom: 78,
    backgroundColor: '#fff',
    borderRadius: 8,
  },
  forecastWeek__title: {
    marginBottom: 32,
    fontSize: 32,
    fontWeight: 'bold',
  },
  forecastWeek__wrapper: {
    borderWidth: 2,
    backgroundColor: 'rgba(128, 131, 164, 0.06)',
    borderColor: 'rgba(128, 131, 164, 0.2)',
    marginBottom: 20,
    borderRadius: 8,
  },
  forecastWeek__menu: {},
  select: {
    width: 252,
    height: 48,
    fontWeight: '400',
    borderColor: 'black',
    borderRadius: 8,
  },
});
